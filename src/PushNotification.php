<?php

namespace Yogesh\Notification;

class PushNotification
{
    public $serverKey;
    
    public function __construct($serverKey)
    {
        $this->serverKey = $serverKey;
    }

    /**
     *  Send push notification to from firebase
     */
    public function sendPushNotification($token, $title, $body = null, $data = [])
    {
        $serverKey = $this->serverKey;
        try {
            $url = "https://fcm.googleapis.com/fcm/send";

            $notification = ['title' => $title, 'body' => $body, 'sound' => 'default', 'badge' => '1'];
            $arrayToSend = ['to' => $token, 'notification' => $notification, 'priority' => 'high'];

            if (count($data)) {
                $arrayToSend['data'] = $data;
            }

            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key=' . $serverKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result= curl_exec($ch);
            curl_close($ch);

            $resultData = ['success' => true, 'message' => $result];
            // $resultData = json_decode($result, true);
            
        } catch (\Exception $e) {
            $resultData = ['success' => false, 'message' => $e->getMessage()];
        }

        return $resultData;
    }

    public function sendPushBulKNotication(array $tokens = [], $title, $description = null, $data = null)
    {
        $serverKey = $this->serverKey;
        try {
            $url = "https://fcm.googleapis.com/fcm/send";

            if ($data) {
                $notification = array('title' => $title, 'body' => $description, 'sound' => 'default', 'content-available' => 1);
            } else {
                $notification = array('title' => $title, 'body' => $description, 'sound' => 'default', 'badge' => '1');
            }

            $arrayToSend = array('registration_ids' => $tokens, 'notification' => $notification, 'priority' => 'high', 'data' => $data);
            $json = json_encode($arrayToSend);

            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key=' . $serverKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);

            $resultData = ['success' => true, 'message' => $result];
            // $resultData = json_decode($result, true);

        } catch (\Exception $e) {
            $resultData = ['success' => false, 'message' => $e->getMessage()];
        }

        return $resultData;
    } 
}