
## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
your_Laravel_project/
vendor/
```


## Install

Via Composer

``` bash
$ composer require yogesh/push-notification
```

## Usage

``` php laravel 

use Yogesh\Notification\PushNotification;

```
## Server Key
    $serverKey = 'server_key'; // define fpm server key 

## Notification Object
    $notification = new PushNotification($serverKey); // make notification object

## Call Methods of Class  
    $notification->sendPushNotification($device_token ,$title,$body); // pass device token of particular user  
    $notification->sendPushBulKNotication($device_tokens ,$title ,$body); // pass device tokens of multiple users in array


## Security

If you discover any security related issues, please email yrana8786@gmail.com instead of using the issue tracker.

## Credits

- [kumarashok30592@gmail.com]

